import React from "react";
import { HashRouter, Routes, Route } from "react-router-dom";
import ProtectedRoute from "./Routes/ProtectedRoute";
import LoginPage from "./Auth/LoginPage/LoginPage";
import ErrorPage from "./Auth/ErrorPage/ErrorPage";
import Dashboard from "./Components/Dashboard/Dashboard";
import "./App.css";
function App() {
  return (
    <div className="App">
      <HashRouter>
        <Routes>
          <Route index path="/" element={<LoginPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="*" element={<ErrorPage />} />
          <Route
            path="/dashboard"
            element={<ProtectedRoute>{<Dashboard />}</ProtectedRoute>}
          />
        </Routes>
      </HashRouter>
    </div>
  );
}
export default App;
