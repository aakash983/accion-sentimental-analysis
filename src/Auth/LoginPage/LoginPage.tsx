import React from "react";
import { useNavigate } from "react-router-dom";
import {
  GoogleOAuthProvider,
  GoogleLogin,
  CredentialResponse,
} from "@react-oauth/google";
import {message} from "antd";

const LoginPage: React.FC = () => {
  const navigate = useNavigate();
  const loginSuccess = (credentialResponse: CredentialResponse) => {
    if (credentialResponse.credential) {
      const tokenPayload = JSON.parse(
        atob(credentialResponse.credential.split(".")[1])
      );

      if (
        tokenPayload.email &&
        tokenPayload.email.endsWith("@accionlabs.com")
      ) {
        console.log("Login Success:", credentialResponse);
        localStorage.setItem('user', JSON.stringify({
          name: tokenPayload.name,
          picture: tokenPayload.picture,
          email: tokenPayload.email
        }));
        message.success("Login SuccessFull!")
        navigate("/dashboard");
      } else {
        message.error("Only @accionlabs.com email addresses are allowed.");
      }
    } else {
      message.error("Invalid credential response.");
    }
  };
  const loginError = () => {
    message.error("Login Failed");
  };

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div style={{ marginTop: "30px" }}>
        <GoogleOAuthProvider clientId="136393195060-5ba2grphv6ejklpraeodettqcvv2k8ji.apps.googleusercontent.com">
          <GoogleLogin
            onSuccess={loginSuccess}
            onError={loginError}
            useOneTap
          />
        </GoogleOAuthProvider>
      </div>
    </div>
  );
};
export default LoginPage;