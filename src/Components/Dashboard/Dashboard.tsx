import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import {
  Upload,
  Button,
  Table,
  Layout,
  Spin,
  Menu,
  Dropdown,
  Avatar,
  Typography,
  Input,
  Modal,
  List,
  Form,
  Select,
  notification,
  Tag,
  Row,
  Card,
  Col,
  Space,
  message,
  Divider,
} from "antd";
import {
  CloudUploadOutlined,
  SettingOutlined,
  PlusOutlined,
  CloseOutlined,
  DownloadOutlined,
  MinusCircleOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import accion from "../../Assets/images/logo.png";
import "./Dashboard.css";
import type { UploadFile } from "antd/es/upload/interface";
import type { RcFile } from "antd/lib/upload/interface";
import type { InputRef } from "antd";

const { Header, Content, Footer } = Layout;
const { Text } = Typography;
const { Item } = Form;
const { Option } = Select;
interface Category {
  [key: string]: string[];
}
interface FormData {
  column: string;
  rows: string[];
}
interface FileDataType {
  key: number;
  fileName: string;
  columns: string[];
  columnsWithFlag: ColumnWithFlag[];
  file_path: string;
}
interface Response {
  "Company Name": boolean;
  Events: boolean;
  Likes: boolean;
  Enhance: boolean;
}
interface ColumnWithFlag {
  name: string;
  flag: boolean;
}
const Dashboard: React.FC = () => {
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem("user") || "{}");
  const [name, setName] = useState("");
  const inputRef = useRef<InputRef>(null);
  const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const handleLogout = () => {
    localStorage.removeItem("user");
    message.success("Logout Successfully!");
    navigate("/");
  };
  const [data, setData] = useState<FileDataType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [selectedColumns, setSelectedColumns] = useState<string[]>([]);
  const [selectedData, setSelectedData] = useState<any[]>([]);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [reportData, setReportData] = useState<any[]>([]);
  const handleUpload = async (file: RcFile) => {
    setFileList([file]);
    const formData = new FormData();
    formData.append("file", file);
    try {
      const response = await fetch(
        "https://intranet.accionlabs.com/sentimentapi/uploadfile/",
        //"http://192.168.168.50:8010/uploadfile/",
        {
          mode: "cors",
          method: "POST",
          body: formData,
        }
      );
      if (!response.ok) {
        throw new Error("Failed to upload file");
      }
      const result = await response.json();
      const { fileName, columns, file_path, sentiment_status } = result;
      const columnsWithFlag: ColumnWithFlag[] = columns.map(
        (column: string) => {
          return {
            name: column,
            flag: sentiment_status[column] || false,
          };
        }
      );
      console.log(columnsWithFlag);
      console.log(columns);
      const newFileData: FileDataType = {
        key: data.length,
        fileName,
        columns,
        columnsWithFlag,
        file_path,
      };
      setData((prevData) => [...prevData, newFileData]);
    } catch (error) {
      console.error("Error uploading file:", error);
    }
    return false;
  };
  const columns = [
    {
      title: "Uploaded File(s)",
      dataIndex: "fileName",
      key: "fileName",
    },
    {
      title: "Select Columns to Extract",
      dataIndex: "columnsWithFlag",
      key: "columnsWithFlag",
      render: (columnsWithFlag: ColumnWithFlag[]) => (
        <div>
          {columnsWithFlag.map((col) => (
            <span key={col.name}>
              {col.flag ? (
                <Tag
                  color={
                    selectedColumns.includes(col.name) ? "blue" : "default"
                  }
                  onClick={() => handleSelectColumn(col.name)}
                  style={{ cursor: "pointer", margin: "2px" }}
                >
                  {col.name}
                </Tag>
              ) : (
                <Tag
                  style={{
                    cursor: "pointer",
                    margin: "2px",
                    color: "darkgrey",
                  }}
                >
                  {col.name}
                </Tag>
              )}
            </span>
          ))}
        </div>
      ),
    },
  ];

  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleSelectColumn = (column: string) => {
    setSelectedColumns((prevColumns) =>
      prevColumns.includes(column)
        ? prevColumns.filter((col) => col !== column)
        : [...prevColumns, column]
    );
  };
  const handleGenerateReport = async () => {
    const selectedData = data.map((fileData) => {
      const selectedColumnsData = fileData.columns.filter((col) =>
        selectedColumns.includes(col)
      );
      return {
        file_path: fileData.file_path,
        columns: selectedColumnsData,
      };
    });
    try {
      const formData = new FormData();
      // formData.append("categories","HR");
      formData.append("file_path", fileList[0] as RcFile);
      // `http://192.168.168.50:8010/sentiments?columns=${selectedData
      const response = await fetch(
        `https://intranet.accionlabs.com/sentimentapi/sentiments?columns=${selectedData
          .map((d) => d.columns)
          .join("%")}`,
        {
          method: "POST",
          body: formData,
        }
      );
      if (!response.ok) {
        throw new Error("Failed to generate report");
      }
      const result = await response.json();
      setSelectedData(selectedData);
      message.success("File uploaded successfully! Generating report...");
      setReportData(result);
    } catch (error) {
      console.error("Error generating report:", error);
      message.error("Failed to generate report");
    }
  };
  const generateExcelReport = (data: any[]) => {
    const ws = XLSX.utils.json_to_sheet(data);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Report");
    const wbout = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    saveAs(
      new Blob([wbout], { type: "application/octet-stream" }),
      "report.xlsx"
    );
  };
  const downloadReport = () => {
    if (reportData.length === 0) {
      message.error(
        "No report data available. Please upload a file and select a column first."
      );
      return;
    }
    generateExcelReport(reportData);
  };
  const selectedColumnsTableColumns = [
    {
      title: "File Name",
      dataIndex: "fileName",
      key: "fileName",
    },
    {
      title: "Selected Columns",
      dataIndex: "columns",
      key: "columns",
      render: (data: any[]) => (
        <div>
          {data.map((col, index) => (
            <Tag
              key={index}
              color="blue"
              closable
              onClose={() => handleSelectColumn(col)}
            >
              {col}
            </Tag>
          ))}
        </div>
      ),
    },
  ];
  const [data2, setData2] = useState<Category>({});
  const [newItem, setNewItem] = useState<string>("");
  const addItem = (
    e: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>
  ) => {
    e.preventDefault();
    setName("");
    setTimeout(() => {
      inputRef.current?.focus();
    }, 0);
  };
  useEffect(() => {
    const fetchData = async () => {
      //  "http://192.168.168.50:8010/get_categories"
      try {
        const response = await fetch(
          "https://intranet.accionlabs.com/sentimentapi/get_categories"
        );
        const result = await response.json();
        setData2(result);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    const postData = async () => {
      try {
        // http://192.168.168.50:8010/get_categories
        if (newItem) {
          await fetch(
            "https://intranet.accionlabs.com/sentimentapi/get_categories",
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({ name: newItem }),
            }
          );
          setNewItem("");
          fetchData();
        }
      } catch (error) {
        console.error("Error posting data:", error);
      }
    };
    fetchData();
    if (newItem) {
      postData();
    }
  }, [newItem]);
  const removeItem = (category: string, item: string) => {
    setData2((prevData) => {
      const newData = { ...prevData };
      newData[category] = newData[category].filter((i) => i !== item);
      return newData;
    });
  };

  const [formData, setFormData] = useState<FormData>({
    column: "",
    rows: [""],
  });
  const handleAddRow = () => {
    setFormData({ ...formData, rows: [...formData.rows, ""] });
  };
  const handleRemoveRow = (index: number) => {
    const newRows = formData.rows.filter((_, rowIndex) => rowIndex !== index);
    setFormData({ ...formData, rows: newRows });
  };
  const handleColumnChange = (value: string) => {
    setFormData({ ...formData, column: value });
  };
  const handleRowChange = (
    index: number,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newRows = [...formData.rows];
    newRows[index] = event.target.value;
    setFormData({ ...formData, rows: newRows });
  };
  const handleSubmit = async () => {
    setLoading(true);
    try {
      // http://192.168.168.50:8010/add_categories
      const response = await fetch(
        "https://intranet.accionlabs.com/sentimentapi/add_categories",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(formData),
        }
      );

      if (response.ok) {
        setFormData({
          column: "",
          rows: [""],
        });
        notification.success({
          message: "Success",
          description: "Data submitted successfully",
        });
      } else {
        console.error("Error submitting data:", response.statusText);
        notification.error({
          message: "Error",
          description: "Error submitting data",
        });
      }
    } catch (error) {
      console.error("Error submitting data:", error);
      notification.error({
        message: "Error",
        description: "Error submitting data",
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <Layout>
      <Header style={{ background: "#e0e0e0", alignItems: "center" }}>
        <Row align="middle" justify="space-between">
          <img
            src={accion}
            style={{ width: 200, height: 40, margin: "16px 0" }}
            alt="Accionlabs"
          />
          <Space>
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item key="logout">
                    <Button onClick={handleLogout}>Logout</Button>
                  </Menu.Item>
                </Menu>
              }
              trigger={["click"]}
            >
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                <Avatar
                  src={user?.picture}
                  size="large"
                  style={{ marginRight: 10 }}
                />
                <Text style={{ marginRight: 20 }}>{user?.name}</Text>
              </a>
            </Dropdown>
          </Space>
        </Row>
      </Header>

      <Content style={{ padding: "0 48px" }}>
        <Row justify="end" align="middle" className="configure-text">
          <div
            style={{
              width: 350,
              textAlign: "center",
              padding: "20px",
              // border: '1px solid',
              // boxShadow: '0 4px 8px rgba(0,0,0,0.1)',
              borderRadius: "8px",
            }}
          >
            <Col>
              <Button type="text" onClick={showModal}>
                <SettingOutlined /> Configure Keywords
              </Button>

              <Text
                style={{
                  display: "block",
                  marginTop: "10px",
                  marginBottom: "20px",
                }}
              >
                <InfoCircleOutlined />
                &nbsp; Explore the keywords used for our sentiment analysis by
                clicking here to find out more.
              </Text>
            </Col>
          </div>
        </Row>
        <Modal
          bodyStyle={{ overflowY: "auto", maxHeight: "calc(100vh - 200px)" }}
          title="Configure Keywords"
          visible={isModalVisible}
          onCancel={handleCancel}
          footer={
            [
              // <Button key="cancel" onClick={handleCancel}>
              //   Cancel
              // </Button>,
              // <Button key="save" type="primary" onClick={handleSave}>
              //   Save
              // </Button>,
            ]
          }
        >
          <div>
            <Spin spinning={loading}>
              <Form
                layout="vertical"
                onFinish={handleSubmit}
                initialValues={formData}
              >
                <p className="red">*Keywords must be in alphabets</p>
                <Row gutter={16} align="middle">
                  <Col span={12}>
                    <Item
                      //label="Add Keywords"
                      name="column"
                      rules={[
                        { required: true, message: "Column is required" },
                      ]}
                    >
                      <Select
                        // mode="tags"
                        optionFilterProp="label"
                        showSearch
                        placeholder="Select"
                        value={formData.column}
                        onChange={handleColumnChange}
                        style={{ width: "100%" }}
                        dropdownRender={(menu) => (
                          <>
                            {menu}
                            <Divider style={{ margin: "8px 0" }} />
                            <Space style={{ padding: "0 8px 4px" }}>
                              <Input
                                placeholder="Please enter item"
                                ref={inputRef}
                                value={name}
                                onChange={onNameChange}
                                onKeyDown={(e) => e.stopPropagation()}
                              />
                              <Button
                                type="text"
                                icon={<PlusOutlined />}
                                onClick={addItem}
                              >
                                Add item
                              </Button>
                            </Space>
                          </>
                        )}
                      >
                        {Object.keys(data2).map((option, index) => (
                          <Option key={index} value={option}>
                            {option}
                          </Option>
                        ))}
                      </Select>
                    </Item>
                  </Col>
                  {formData.rows.map((row, index) => (
                    <Col span={6} key={index}>
                      <Item
                        //   label={`Row ${index + 1}`}
                        name={`row${index}`}
                        rules={[
                          { required: true, message: "Row value is required" },
                        ]}
                      >
                        <Space>
                          <Input
                            placeholder={`Row ${index + 1}`}
                            value={row}
                            onChange={(event) => handleRowChange(index, event)}
                          />
                          {formData.rows.length > 1 && (
                            <MinusCircleOutlined
                              onClick={() => handleRemoveRow(index)}
                            />
                          )}
                        </Space>
                      </Item>
                    </Col>
                  ))}
                  <Col span={6}>
                    <Button
                      type="dashed"
                      onClick={handleAddRow}
                      icon={<PlusOutlined />}
                      style={{ width: "100%", marginBottom: 16 }}
                    >
                      Add Row
                    </Button>
                  </Col>
                  <Col span={6}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ width: "100%", marginBottom: 16 }}
                    >
                      Add Data
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Spin>
            <div style={{ padding: "20px" }}>
              {Object.keys(data2).map((category) => (
                <Card
                  title={category}
                  key={category}
                  style={{ marginBottom: "20px" }}
                >
                  <List
                    bordered
                    dataSource={data2[category]}
                    renderItem={(item) => (
                      <Button
                        key={item}
                        style={{ margin: "5px" }}
                        type="default"
                        icon={<CloseOutlined />}
                        iconPosition="end"
                        onClick={() => removeItem(category, item)}
                      >
                        {item}
                      </Button>
                    )}
                  />
                </Card>
              ))}
            </div>
          </div>
        </Modal>
        <Row className="aligh-txt" justify="center" align="middle">
          <Col>
            <h3>Start Exploring Sentimental Analysis</h3>
          </Col>
        </Row>
        <Row justify="center" align="middle">
          <Col style={{ margin: "16px 0" }}>
            <Upload
              accept=".xls, .xlsx"
              beforeUpload={handleUpload}
              showUploadList={false}
              multiple
            >
              <Button type="primary" icon={<CloudUploadOutlined />}>
                Upload File
              </Button>
            </Upload>
          </Col>
        </Row>
        <Table
          columns={columns}
          dataSource={data}
          pagination={false}
          style={{ marginTop: 20 }}
        />
        <Row justify="center" style={{ marginTop: "10px" }}>
          <Col>
            {/* {valid === true && */}
            <Button type="primary" onClick={handleGenerateReport}>
              Generate Report
            </Button>
            {/* } */}
          </Col>
        </Row>
        {selectedData.length > 0 && (
          <div>
            <h4>Selected Columns(s)</h4>
            <Table
              columns={selectedColumnsTableColumns}
              dataSource={selectedData}
              pagination={false}
              style={{ marginTop: 20 }}
            />
            <Row justify="center" style={{ marginTop: "20px" }}>
              <Col>
                <Button
                  type="primary"
                  icon={<DownloadOutlined />}
                  onClick={downloadReport}
                >
                  Download Report
                </Button>
              </Col>
            </Row>
          </div>
        )}
      </Content>
      <Footer style={{ textAlign: "center" }}>
        ©{new Date().getFullYear()} Created by AccionLabs
      </Footer>
    </Layout>
  );
};
export default Dashboard;
